const Express = require("express")
const path = require("path")
const spawn = require("child_process").spawn
const formidable = require("formidable")
let router = Express.Router()

//upload directory:
const root = path.join(__dirname, '../uploads/');
const pythonModule = path.join(__dirname, '../pythonModule/')

router.post("/upload", (req, res) => {
    req.headers['if-none-match'] = 'no-match-for-this'; //prevent express from sending "304 not modified" code which bugs the working of this api route.
    if (!req.get('Content-Type').startsWith('multipart/form-data'))
        return res.status(401).end();
    //uploading process:
    new formidable.IncomingForm().parse(req).on('fileBegin', (name, file) => {
        file.path = root + file.name
    }).on('file', (name, file) => {
        // calling our pythong modile with our uploaded file path:
        var process = spawn('python', [pythonModule + "deff.py", file.path]);
        process.stdout.on('data', function (data) {
            res.send(data.toString());
        })
    });
})

router.get('*', (req, res) => res.status(404).end());

module.exports = router;