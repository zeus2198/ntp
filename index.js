const express = require("express")
const api = require("./api")

var app = express()

app.use("/api", api)
app.use('/public', express.static('public'))

app.get('/', function (req, res) {
   res.send('App is running!')
})

var server = app.listen(8081, function () {
   console.log("App listening at http://%s:%s", server.address().address, server.address().port )
})